package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "os"
	"sort"
	"time"
	"strings"
	"encoding/json"
)

type Fpmstatus struct {
	Ip string
	PrevAcceptedConn int
	AcceptedConn int `json:"accepted conn"`
	ListenQueue int `json:"listen queue"`
	MaxListenQueue int `json:"max listen queue"`
	ListenQueueLen int `json:"listen queue len"`
	IdleProcesses int `json:"idle processes"`
	ActiveProcesses int `json:"active processes"`
	TotalProcesses int `json:"total processes"`
	MaxActiveProcesses int `json:"max active processes"`
	SlowRequests int `json:"slow requests"`
	Processes []struct { 
		Pid int `json:"pid"`
		State string `json:"state"`
		RequestURI string `json:"request uri"`
		RequestDuration int `json:"request duration"`
		RequestMemory int `json:"last request memory"`
    }
}

const (
	colorReset = "\033[0m"
	colorRed = "\033[31m"
	colorGreen = "\033[32m"
	colorDarkGreen = "\033[1;32m"
	colorYellow = "\033[33m"
	colorBlue = "\033[34m"
	colorPurple = "\033[35m"
	colorCyan = "\033[36m"
	colorWhite = "\033[37m"
)

func main() {

	var s Fpmstatus					
	d := make(map[string]Fpmstatus)
	var parts []string

	s.Ip="185.43.204.117"
	d["frontend0"] = s

	s.Ip="185.43.204.118"
	d["frontend1"] = s

	s.Ip="185.43.204.114"
	d["frontend3"] = s

	for {

		for host, s := range d {
			response, err := http.Get("http://"+s.Ip+"/php_status_manna?json&full")
			if err != nil {
				fmt.Print(err.Error())
				os.Exit(1)
			}
		
			responseData, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Fatal(err)
			}

			// save previous value of AcceptedConn
			s.PrevAcceptedConn = s.AcceptedConn

			json.Unmarshal([]byte(responseData), &s)

			// if this is the first time, then prev = current 
			if s.PrevAcceptedConn == 0 {
				s.PrevAcceptedConn = s.AcceptedConn
			}

			// cut the request URI after & for privacy and security reasons (payment links)
			for i, _ := range s.Processes {
				parts = strings.Split(s.Processes[i].RequestURI, "&")
				s.Processes[i].RequestURI = parts[0]
			}
			d[host] = s
		}

		// go to upper left corner of the screen
		fmt.Print("\033[0;0H")
		fmt.Print("\033[2J")

		fmt.Println("\n---------====== Top 5 expensive executed PHP processes per frontend: ======-------------------------------------------------------------------")

		// map is unordered, sort map keys
		keys := make([]string, 0, len(d))
		for k := range d {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		// read map by ordered key
		for _, host := range keys {
			sort.Slice(d[host].Processes, func(i, j int) bool {
				return d[host].Processes[i].RequestDuration > d[host].Processes[j].RequestDuration
			})

			fmt.Println()

			// display top 5 processes with longest run
			for i, q := range d[host].Processes {
				if i<5 {
					fmt.Printf("%s  %6d ms  %4d MB   %.110s\n", host, q.RequestDuration/1000, q.RequestMemory/1024/1024, q.RequestURI)
				}
			}
		}

		fmt.Println()

		for _, k := range keys {

			// display gauge
			fmt.Print(k, " [")
			for i := 0; i < d[k].TotalProcesses; i++ {
				switch {
					case i < d[k].ActiveProcesses-1:
						if i > d[k].TotalProcesses / 4 -1 {
							fmt.Print(string(colorRed),"█")
						} else {
							fmt.Print(string(colorGreen),"█")
						}	
					case i < d[k].MaxActiveProcesses:
						fmt.Print(string(colorYellow), "=")
					default:
						fmt.Print(string(colorReset), "-")
				} 
			}
			fmt.Printf("] "+string(colorGreen)+"%2d"+string(colorReset)+"/"+string(colorYellow)+"%2d"+string(colorReset)+"/%2d  Conns: %7d (%3d/s) LQ: %3d\n", d[k].ActiveProcesses-1, d[k].MaxActiveProcesses, d[k].TotalProcesses, d[k].AcceptedConn, d[k].AcceptedConn-d[k].PrevAcceptedConn, d[k].ListenQueue)
		}

		fmt.Println("\n---------====== Running PHP processes: ======-------------------------------------------------------------------------------------------------\n")

		for _, host := range keys {
			for _, q := range d[host].Processes {
				if q.State == "Running" && q.RequestURI != "/php_status_manna?json" {
					fmt.Printf("%s  %6d ms            %.110s\n", host, q.RequestDuration/1000, q.RequestURI)
				}
			}
		}

		time.Sleep(1 * time.Second) 
	} 
}